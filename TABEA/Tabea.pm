#!/usr/opkg/bin/perl -w
##
##  OSSP tabea - Web Configuration Editor
##  The Tabea Perl Module
##
##  Copyright (c) 2001-2002 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2001-2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
##
##  This file is part of OSSP tabea, a web configuration editor
##  which can be found at http://www.ossp.org/pkg/tool/tabea/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  Tabea.pm: Tabea Module
##


package TABEA::Tabea;
require 5.000;
use IO::File;

sub new{
    #  Currently the only supported $type is "HTML2" which causes only
    #  tags described in RFC1866 to be used.  When a menu is rendered,
    #  some elements may require uniqe names in a scope wider than
    #  known by this module, i.e. when two configs are presented on one
    #  screen or along with other information, so every item is
    #  prefixed with $prefix.
    my $proto = shift;
    my $class = ref($proto) || $proto || "TABEA::Tabea.pm" ;
    my ($config, $prefix) = @_;
    my $cfgfh;

    my $self = {};
    $self->{Config} = $config;
    $self->{Prefix} = $prefix;
    $cfgfh = new IO::File ("<".$self->{Config}) || die ;
    while (<$cfgfh>) {
        $self->{Configuration} .= $_;
    }
    $cfgfh->close();
    
    bless($self, $class);
     
    return $self;
}


sub load{
    #  Reads $data and sets the value of items.
    my $self = shift;
    my $rc = 1;

    if (! @_) { $rc = 0 };
    my ($data) = @_;

    print "TABEA::Tabea $data load funtion\n";
 
    return $rc;
}


sub save {
    #  Writes items including their values.
    my $self = shift;
    my $rc = 1;

    if (! @_) { $rc = 0 };
    my ($data) = @_;

    print "TABEA::Tabea $data save funtion\n";
 
    return $rc;

}


sub parse {

    my $self = shift;
    my $rc = 1;

    if (! @_) { $rc = 0 };
    my ($param) = @_;

    print "TABEA::Tabea $param parse funtion\n";
 
    return $rc;
}



sub render {
    #  Renders a menu and writes it into the buffer. The contents of the
    #  buffer can be merged into a larger output. Values are verified
    #  and invalid data is marked (FIXME how? red, reset to default,
    #  configurable behaviour, configurable error messages ...) The
    #  caller must finally print out the menu and when an input comes
    #  back it must identify menu activity (i.e. by checking the
    #  prefix) and call render again and again or execute some action.
    my $self = shift;
    my $rc = 1;

    if (! @_) { $rc = 0 };
    my ($buffer) = @_;

    $buffer .= $self->{Configuration};
    
    return $buffer, $rc;
}

sub import{
    #  Reads a legacy (manually edited or previously exported) file and
    #  tries to match out values.
    my $self = shift;
    my $rc = 1;

    if (! @_) { $rc = 0 };
    my ($legacyfile) = @_;

    my $legacyfh;
    my $newdata = "";

    print "TABEA::Tabea import " . $legacyfile . "funtion\n";

    $legacyfh = new IO::File ("<".$legacyfile) || die ;
    while (<legacyfh>) {
        $newdata .= $_;
    }
    $legacyfh->close();
 
    return $rc;
}



sub export{
    #  Applies variable substitution for a template and writes the
    #  result out to exportfile.
    my $self = shift;
    my $rc = 1;

    if (! @_) { $rc = 0 };
    my ($template, $exportfile) = @_;
    my $exportfh;

    print "TABEA::Tabea $template, $exportfile export funtion\n";

    $exportfh = new IO::File (">".$exportfile) || die "Cannot open $exportfile for writing.";
    print $exportfh $template;
    $exportfh->close();

    return $rc;

}






sub destroy{
    my $self = shift;
    my $rc = 1;


    print "TABEA::Tabea $self destroy funtion\n";
 
    return $rc;
}





1;
