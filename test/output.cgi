#!/usr/opkg/bin/perl -w
#
# This program is for testing the possibilities
# of creating an output in html
# Later test the possibiliies to read in the values
# from an cfg file and convert this to html
#

require 5.000;
use strict;
use CGI;
use IO;
use Data::Dumper;

use lib ".";

use TESTPM::Cfg;


$|++;


#   establish CGI query object
my $cgi = new CGI;
 
#   activate a general error handler
$SIG{__DIE__} = sub {
my ($err) = @_;

    #$err =~ s|at\s+\S+\s+line\s+(\d+)|(line $1)|s;
    $err =~ s|\n|<br>\n|sg;
    print STDOUT $cgi->header(-expires => '+1s') .
    "<title>TabeaTest :: ERROR</title>",
    "<h1>TabeaTest :: ERROR</h1>\n" .
    "<pre>$err</pre>\n";
    exit(0);
};





#   read config

my $filename = "/d1/ps/work/ossp-pkg/tabea/test/tabea-cfg.in";
my $filein = new IO::File "<$filename" || die;
my $config = '';
   $config .= $_ while (<$filein>);
 
$filein->close();

my @resword = ("item", "visible", "type", "value", "checked", "description", "selected");
my @types =("text", "password", "checkbox", "radio", "image", "hidden", "submit", "reset",
"selectmultiple", "textarea");

my $CF = &config_read($filename);
#my $dump = Data::Dumper->Dump([$CF]);
#my $cfgtext = $dump ;

my $dump = Data::Dumper->Dump([$CF]) ;

#print $cfgtext $dump;
#print $cfgtext . Data::Dumper->Dump([$CF]);
#print Data::Dumper->Dump([$CF]);



my @cf = TESTPM::Cfg->parsit($dump);



my $form = '';

for my $name (qw(output results)) {
    my $val = $cgi->param("dialog_$name") || "";
    if ($val ne '') {
        $form = $name;
    }
}




my $page = $cgi->startform( -method => "POST", -action => $cgi->url(-full => 1));

if ($form eq '') {

    $page .= "<br>Config text<br>\n";
#    $page .= "<br>$config<br>\n";
#    $page .= "<br><font color=red>Break</font><br>\n";
#    $page .= "<br>$CF<br>";
#    $page .= "<br><font color=red>Break</font><br>\n";
    $page .= "<br>$dump<br>";
    $page .= "<br><font color=red>Geparster Text</font><br>\n";
#    $page .= "<br>$cfgtext<br>";
#    $page .= "<br>@cf<br>";
    my $cfgtext = Data::Dumper->Dump([@cf]);
#    foreach (@cf) {
#        $page .= "<br>$_<br>";
#    }

    $page .= "<br>$cfgtext<br>";
    $page .= $cgi->submit(-name => 'dialog_output', -value => 'Show output');
}
elsif ($form eq 'output') {
    $page .= "<br><br>We are testing<br>" ;
    $page .= "\n\nHere comes the text output:<br>\n" ;
    $page .= &text();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the password output:<br>\n" ;
    $page .= &password();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the checkbox output:<br>\n" ;
    $page .= &checkbox();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the radio output:<br>\n" ;
    $page .= &radio();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the image output:<br>\n" ;
    $page .= &image();
#    $page .= &hidden();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the submit output:<br>\n" ;
    $page .= &submit();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the reset output:<br>\n" ;
    $page .= &reset();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the select output:<br>\n" ;
    $page .= &select();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the textaraea output:<br>\n" ;
    $page .= &textarea();
    #$page .= "<br><br><br><input type=submit name=dialog_output value=\"Show results\"><br>\n" 
    $page .= $cgi->submit(-name => 'dialog_results', -value => 'Show results');
}
elsif ( $form eq 'results' )  {
    $page .= "<br><br>Here are the results<br>\n";
    $page .= "\n\nHere comes the text result: \n" ;
    $page .= &text_result();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the password result: \n" ;
    $page .= &password_result();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the checkbox result: \n" ;
    $page .= &checkbox_result();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the radio result: \n" ;
    $page .= &radio_result();
    $page .= "<br><br>\n" ;
#    $page .= "\n\nHere comes the image result:<br>\n" ;
#    $page .= &image_result();
#    $page .= &hidden();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the submit result: \n" ;
    $page .= &submit_result();
    $page .= "<br><br>\n" ;
#    $page .= "\n\nHere comes the reset result:<br>\n" ;
#    $page .= &reset_result();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the select result: \n" ;
    $page .= &select_result();
    $page .= "<br><br>\n" ;
    $page .= "\n\nHere comes the textaraea result: \n" ;
    $page .= &textarea_result();

}
else {
     die "no correct dialog found";
}

$page .= $cgi->endform;


#print STDOUT $cgi->header(-cookie => $cookies, -expires => '+1s') . $page;
print STDOUT $cgi->header() . $page;

sub text{
    my $text;    
    # Text Field: INPUT TYPE=TEXT

    # <p>Street Address: <input name=street><br>
    # Postal City code: <input name=city size=16 maxlength=16><br>
    # Zip Code: <input name=zip size=10 maxlength=10 value="99999-9999"><br>
   
    $text .= "<p>Street Address: <input name=street><br>" .
    "Postal City code: <input name=city size=16 maxlength=16><br>" .
    "Zip Code: <input name=zip size=10 maxlength=10 value=\"99999-9999\"><br>" ;
 

    #item {  
    #   name street
    #   visible "Street Address:"
    #    type text
    #}
    #item {  
    #    name city
    #    visible "Postal City code:"
    #    type text[16][16]
    #}
    #item {  
    #    name zip
    #    visible "Zip Code:"
    #    type text[10][10]
    #    value {
    #        name "99999-9999"
    #    }
    #}
    return $text;
}

sub text_result{
    my $text;

    $text .= $cgi->param('street') . "<br>" ;

    return $text;
}


sub password {
    my $text;
#    # Password Field: INPUT TYPE=PASSWORD

    # <p>Name: <input name=login> Password: <input type=password name=passwd>

    $text .= "<p>Name: <input name=login> Password: <input type=password name=passwd>" ;

#    item {  
#        name login
#        visible "Name:" 
#        type text
#    }
#    item {  
#        name passwd
#        visible "Password:"
#        type password
#    }
#
#
      return $text;
}

sub password_result{
    my $text;

    $text .= $cgi->param('passwd') . "<br>" ;

    return $text;
}

sub checkbox {
    my $text;
#    # Check Box: INPUT TYPE=CHECKBOX
#
#    # <p>What flavors do you like?
##    # <input type=checkbox name=flavor value=vanilla checked>Vanilla<br>
    # <input type=checkbox name=flavor value=strawberry>Strawberry<br>
#    # <input type=checkbox name=flavor value=chocolate checked>Chocolate<br>

    $text .= "<p>What flavors do you like?\n" .
    "<input type=checkbox name=flavor value=vanilla checked>Vanilla<br>\n" .
    "<input type=checkbox name=flavor value=strawberry>Strawberry<br>\n" .
    "<input type=checkbox name=flavor value=chocolate checked>Chocolate<br>\n" ;
#
#    item {
#        name flavor
#        visible "What flavors do you like?"
#        type checkbox
#        value {
#            name vanilla
#            visible "Vanilla"
#            checked
#        }
#        value {
#            name strawberry
#            visible "Strawberry"
#        }
#        value {
#            name chocolate
#            visible "Chocolate"
#            checked
#        }
#    }
#
     return $text;
}

sub checkbox_result{
    my $text;

    $text .= $cgi->param('flavor') . "<br>" ;

    return $text;
}

sub radio {
    my $text;
#    # Radio Button: INPUT TYPE=RADIO
#
#    # <p>Which is your favorite?
#    # <input type=radio name=flavor value=vanilla checked>Vanilla<br>
#    # <input type=radio name=flavor value=strawberry>Strawberry<br>
#    # <input type=radio name=flavor value=chocolate>Chocolate<br>

    $text .= "<p>Which is your favorite?\n" .
    "<input type=radio name=flavor value=vanilla checked>Vanilla<br>\n" .
    "<input type=radio name=flavor value=strawberry>Strawberry<br>\n" .
    "<input type=radio name=flavor value=chocolate>Chocolate<br>\n" ;
#
#    item {
#        name flavor
#        visible "Which is your favorite?"
#        type radio
#        value {
#            name vanilla
#            visible "Vanilla"
#            checked
#        }
#        value {
#            name strawberry
#            visible "Strawberry"
#        }
#        value {
#            name chocolate
#            visible "Chocolate"
#        }
#    }
#
#
     return $text;
}

sub radio_result{
    my $text;

    $text .= $cgi->param('flavor') . "<br>" ;

    return $text;
}

sub image {
    my $text;
#    # Image Pixel: INPUT TYPE=IMAGE
#
#    # <input type=image name=point value=map.gif>
    
#    $text .= "<input type=image name=point value=\"./1.gif\">\n" ;
    $text .= "<img alt=\"gif not found\" src=\"/ossp-cgi/test/1.gif\">\n";
#
#    item {
#        name point
#        visible "Choose a point on the map:"
#        type image
#        value {
#            name "map.gif"
#        }
#    }
#
#
     return $text;
}


sub hidden {
    my $text;
#    # Hidden Field: INPUT TYPE=HIDDEN (FIXME do we really need this?)
#
#    # <input type=hidden name=context value="l2k3j4l2k3j4l2k3j4lk23">

    $text .= "<input type=hidden name=context value=\"l2k3j4l2k3j4l2k3j4lk23\">\n" ;
#
#    item {
#        name context
#        type hidden
#        value {
#            name "l2k3j4l2k3j4l2k3j4lk23"
#        }
#    }
#
#
     return $text;
}

sub submit {
    my $text;
#    # Submit Button: INPUT TYPE=SUBMIT
#
#    # <input type=submit name=recipient value=internal><br>
#    # <input type=submit name=recipient value=world>

    $text .= "<input type=submit name=recipient value=internal><br>\n" .
    "<input type=submit name=recipient value=world>\n" ;


#
#    item {
#        name recipient
#        type submit
#        value {
#            name internal
#        }
#        value {
#            name world
#        }
#    }
#
     return $text;
}

sub submit_result{
    my $text;

    $text .= $cgi->param('recipient') . "<br>" ;

    return $text;
}

sub reset {
    my $text;
#
#    # Reset Button: INPUT TYPE=RESET
#
#    #  <input type=reset>

    $text .= "<input type=reset>\n" ;
#
#    item {
#        description "You may clear the form and start over at any time:"
#        type reset
#    }
     return $text;
}

sub select {
    my $text;
#    # Selection: SELECT
#    # Option: OPTION
#
#    # <SELECT NAME="flavor" SIZE="2">
#    # <OPTION>Vanilla
#    # <OPTION>Strawberry
#    # <OPTION value="RumRasin">Rum and Raisin
#    # <OPTION selected>Peach and Orange
#    # </SELECT>
#

    $text .= "<SELECT NAME=\"flavor\" SIZE=\"2\">\n" .
    "<OPTION>Vanilla\n" .
    "<OPTION>Strawberry\n" .
    "<OPTION value=\"RumRasin\">Rum and Raisin\n" .
    "<OPTION selected>Peach and Orange\n" .
    "</SELECT>\n" ;


#    item {
#        name flavor
#        type select[2] //or type selectmultipe[2]
#        value {
#            name "Vanilla"
#        }
#        value {
#            name "Strawberry"
#        }
#        value {
#            name RumRasin
#            visible "Rum and Raisin"
#        }
#        value {
#            name "Peach and Orange"
#            selected
#        }
#
#    }
#
#
     return $text;
}

sub select_result{
    my $text;

    $text .= $cgi->param('flavor') . "<br>" ;

    return $text;
}

sub textarea {
    my $text;
#    # Text Area: TEXTAREA
#
#    # <TEXTAREA NAME="address" ROWS=6 COLS=64>
#    # HaL Computer Systems
#    # 1315 Dell Avenue
#    # Campbell, California 95008
#    # </TEXTAREA>
#
    $text .=  "<TEXTAREA NAME=\"address\" ROWS=6 COLS=64>\n" .
    "HaL Computer Systems\n" .
    "Dell Avenue\n" .
    "Campbell, California 95008\n" .
    "</TEXTAREA>\n" ;

#    item {
#        name "address"
#        type textarea[64][6]
#        value {
#            name "HaL Computer Systems\n1315 Dell Avenue\nCampbell, California 95008"
#        }
#    }
#
     return $text;
}

sub textarea_result{
    my $text;

    $text .= $cgi->param('address') . "<br>" ;

    return $text;
}







 
#   read configuration
sub config_read {
    my ($file) = @_;
 
    #     read file into memory
    my $fh = new IO::File "<$file" || die;
    my $config = '';
    $config .= $_ while (<$fh>);
 
    $fh->close();
 
    ##  Class method usage
    my $cf = TESTPM::Cfg->parsit($config);
 
 
    ## Object method usage
 
    #my $te=new PM2::Cfg();
    #my $cf=$te->parsit($config);
 
    return $cf;
}
