#!/usr/opkg/bin/perl -w
#
# This is for testing the read in of
# configurationg files
#


require 5.000;
use strict;
use IO;


my $cfgfilename;


if (0 > $#ARGV) {
      usage();
}




$cfgfilename=$ARGV[0];

print "\nWorking with: $cfgfilename\n";

my $filename = "/d1/ps/work/ossp-pkg/tabea/test/tabea-cfg.in";
my $filein = new IO::File "<$cfgfilename" || die;
my $config = ''; 

if (defined $filein) {
    LOOP: while(<$filein>) {
        $_ =~ s|^\s+(.*?)|{$_=$1}|es;       # Remove whitespaces at the beginning
        next LOOP if $_ =~ /^\n/;           # Remove the first empty require
        next LOOP if $_ eq "";              # Remove empty line
        next LOOP if $_ =~/^#/;             # Remove comment lines
 
        $_ =~ s|^([^#]+)#(.*)$|{$_=$1}|es;  # Remove comments on the end
        $_ =~ s|(.*?)\s+$|{$_=$1}|es;       # Remove whitespaces at the end
 
        #($key, $content) =  /^(\w+)\s+(.*)$/;
 
        #$cfghash{$key} = $content;
        $config .= $_;
        
    }
} else {
         die "Can't open config file";
}
#$config .= $_ while (<$filein>);

$filein -> close ;


print "\nHere is the output:\n\n";
print $config;

#################################################################################################
sub usage {
#################################################################################################

    print "\nread-cfg.pl cfgfilename\n\n";

    exit 1;
#################################################################################################
}
