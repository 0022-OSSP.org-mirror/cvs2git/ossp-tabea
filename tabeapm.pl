#!/usr/opkg/bin/perl -w
##
##  OSSP tabea - Web Configuration Editor
##  Copyright (c) 2001-2002 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2001-2002 Cable & Wireless Deutschland <http://www.cw.com/de/>
##
##  This file is part of OSSP tabea, a web configuration editor
##  which can be found at http://www.ossp.org/pkg/tool/tabea/.
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  tabeapm.pl: For testing the new TABEA::Tabea module
##
 
 
require 5.000;
use strict;
use TABEA::Tabea;

my $buffer = "";
my $buffer2 = "";
my $dummy ;

my $test = TABEA::Tabea->new("./test/tabea-cfg.in1", "HTML2") or die "Irgendwie falsch" ;
my $test2 = TABEA::Tabea->new("./test/tabea-cfg.in2", "HTML2") or die "Irgendwie falsch" ;

if( (($buffer, $dummy) = $test->render($buffer)) ) {
    print "Here comes the buffer1 :\n";
    print $buffer;
} else {
    print "Error\n";
}


if( (($buffer2, $dummy) = $test->render($buffer2)) ) {
    print "Here comes the buffer2:\n";
    print $buffer2;
} else {
    print "Error\n";
}
my $template = "My template";
my $template2 = "My template2";
my $legacyfile = "My legacyfile";
my $legacyfile2 = "My legacyfile2";

$test->load($buffer);
$test2->load($buffer2);
$test->save($buffer);
$test2->save($buffer2);
$test->parse($buffer);
$test->parse($buffer2);
$test2->import($legacyfile);
$test2->import($legacyfile2);
$test->export($template, $legacyfile);
$test2->export($template2, $legacyfile2);
$test->destroy($buffer);
$test->destroy($buffer2);
